package com.zhoudy.plusservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class PlusServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlusServiceApplication.class, args);
    }
}
