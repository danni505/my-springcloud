package com.zhoudy.myspringcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySpringcloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(MySpringcloudApplication.class, args);
    }
}
