# 项目简介
本项目是微服务组件学习项目；

 


# 内容主要包含：

| 微服务角色                 | 对应的技术选型                              |
| --------------------- | ------------------------------------ |
| 注册中心(Register Server) | Eureka                               |
| 服务提供者                 | spring mvc、mysql等       |
| 服务消费者                 | Ribbon消费服务提供者的接口                     |
| 熔断器                   | Hystrix  |
| API Gateway           | Zuul                                 |

 



# 准备

## 环境准备：

| 工具     | 版本或描述                |
| ------ | -------------------- |
| JDK    | 1.8                  |
| IDE    | STS 或者 IntelliJ IDEA |
| Maven  | 3.x                  | 



## 主机规划：

| 项目名称      | 端口   | 描述         |
| ---------------------------------------- | ---- | -------------------   |
| plus-service      | 8081    |     计算服务              |
| eureka-service     | 9000 | eureka注册发现服务                 |
| api-zuul       | 8050 | API网关，网关服务工程，使用Zuul对服务进行分发和权限校验          |




## 服务规划
| 服务名称            |  描述   | 示例 | 
| ---------------------------------------- | ---- | ---- | 
|lt22-mc-cc-eureka   | 注册服务|   http://eureka-670c6a72-8e21-471c-b1bd-26cc3a9afe42.paas.xxx.cn |
|lt22-mc-cc-config   | 配置服务 |  http://config-80b60ad7-35d9-4d6d-b136-e29b1e898de8.paas.xxx.cn |

## 码云地址

dev环境：http://mayun.itc.cn/XYK_HZCardBusiness/LT22_MC02_02_CCACC_DEV.git

---------

# 发布PAAS测试

## 访问地址
### route服务
>
```
 http://lt22-mc-ccroute.paas.xxx.cn/crdCirRoute/getCrdNbrs?
stdCrdNbr=0004392250000650316&cltSysid=0130049725&cCMajCtfNbr=&cCHldCtfNbr=
```

### 网关访问
1. GET方式
> 
``` 
 http://lt22-mc-gateway.paas.cn/api-route/crdCirRoute/getCrdNbrs?
stdCrdNbr=0004392250000650316&cltSysid=0130049725&cCMajCtfNbr=&cCHldCtfNbr=&token=zhoudy
```

2. POST方式
> 
` http://lt22-mc-gateway.paas..cn/api-query/crdCirQuery/getCirsJosnList?token=zhjop `

> 请求参数：
``` {
  "params":{
    "auz":{
      "uid":"CrdCirQuery@LT22",
      "signature":"adfbf538c5c225d158b0726bb3da6c04"
     },
    "paraMap":{
      "crdOrdNbr":"20171119000002500171",
      "stdCrdNbr":"0004392250000650316",
      "cltSysid":"0130049725",
      "cCMajCtfNbr":"01309246197910210032",
      "cCHldCtfNbr":"01309246197910210032"
     }
  }
}
```