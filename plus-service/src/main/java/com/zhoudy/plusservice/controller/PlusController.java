/**
 * @author Administrator
 */
/**
 * @author Administrator
 *
 */
package com.zhoudy.plusservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PlusController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DiscoveryClient disClient;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Integer doAdd(@RequestParam Integer a, @RequestParam Integer b) {
        ServiceInstance instance = disClient.getLocalServiceInstance();
        Integer c = a + b;
        logger.info("/add, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", sum:" + c);
        return c;
    }
}