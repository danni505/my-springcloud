package com.zhoudy.apizuul;

import com.zhoudy.apizuul.filters.PreRequestFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@EnableZuulProxy
@SpringBootApplication
public class ApiZuulApplication {

    public static void main(String[] args) {

        SpringApplication.run(ApiZuulApplication.class, args);
    }


    @Bean
    public PreRequestFilter preRequest(){
        PreRequestFilter preRequest = new PreRequestFilter();
        return preRequest;
    }
}
