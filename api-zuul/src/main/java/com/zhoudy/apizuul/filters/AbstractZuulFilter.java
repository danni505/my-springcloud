package com.zhoudy.apizuul.filters;

import com.netflix.zuul.ZuulFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractZuulFilter extends ZuulFilter {

    protected final static Logger logger = LoggerFactory.getLogger(AbstractZuulFilter.class);


}
